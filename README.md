# Automotion
DA Modellauto mit Sensorik

## Tutorial for using Automotion:
- The Raspberry Pi's credentials have not been changed from the default ones, so you can log in with user `pi` and password `raspberry`.
- You first have to get terminal access to the Raspberry Pi, you can achieve this in many ways. (Connect it to a display and use raspi-config, edit `/etc/wpa_supplicant/wpa_supplicant.conf` on the SD card directly, use a USB to LAN adapter, etc.) --> just look it up
- Then you should connect it to a phone hotspot using raspi-config (`sudo raspi-config`). It will remember any network and reconnect to it automatically.
- Using the terminal access (over SSH if you want to use it without a display connected, you can even do this on your phone directly, using an app like [termux](https://github.com/termux/termux-app)) you can start the program by running `sudo python3 ~/RaspberryPi/main.py`.
- Then the Raspberry Pi will be in data transfer mode (red LED), by pressing the button you can change to data acquisition mode (blue LED). In data transfer mode a USB thumb drive can be plugged in to get the data from the Raspberry Pi onto the drive. If the WiFi connection is lost during driving the live data on your device will stop getting transmitted but the car will still write it to storage.

## Raspberry Pi Setup:
### Installing Raspberry Pi OS:
1. Download Raspberry Pi OS Lite from https://www.raspberrypi.com/software/operating-systems/
2. Use some imaging software to get the OS image onto the SD-Card:
```sh
sudo fdisk -l
sudo cp ./image.img /dev/mmcblkX
```
3. Insert the micro SD card into the Raspberry Pi
4. Login using the default `pi` user with password `raspberry`
5. Get your system up-to-date:
```sh
sudo apt update
sudo apt upgrade
```
6. Use `sudo raspi-config`to enable SSH under "Interface Options"
7. Install python, pip, vim and ranger and uninsall nano
```sh
sudo apt install python pip vim ranger
sudo apt remove nano
```
### Automount USB thumb drive:
1. Install usbmount using apt:
```sh
sudo apt install usbmount
```
2. Change `PrivateMounts` to `no`:
```sh
sudo vim /lib/systemd/system/systemd-udevd.service
--------------------------------------------------
PrivateMounts=no
```
3. Edit `/etc/usbmount/usbmount.conf` to contain all needed file systems and mount with the right permissions:
```sh
sudo vim /etc/usbmount/usbmount.conf
```
Add `ntfs` to `FILESYSTEMS=""` and `-fstype=vfat,gid=users,dmask=0007,fmask=0117` to `FS_MOUNTOPTIONS=""`
